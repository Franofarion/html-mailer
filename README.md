# HTML - Mailer
An app that allows you to send e-mail with your own HTML template using your
gmail account.  <br>
I didn't host the installer yet, but I'm working on it !

## If you want to fork : 
I'm using [electron-forge](https://electronforge.i).
After cloning : <br>
`npm i`
&&
`npm start` to run project 
___________

If you want to package the app : <br>
`npm run package`  <br> 
_________________

If you want to create installer :  <br>
`npm run make`